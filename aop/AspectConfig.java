package kr.ac.kopo.ctc.spring.board.aop;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

//Aspect를 사용하기 위한 파일 (AOP: 관심사 분리)
@Configuration
//람다식에 해당하는 프록시
@EnableAspectJAutoProxy
public class AspectConfig {

}
