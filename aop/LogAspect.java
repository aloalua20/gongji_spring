package kr.ac.kopo.ctc.spring.board.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
	
	private static final Logger logger = LoggerFactory.getLogger(LogAspect.class);
	
	//Service에 포함된 *(모든 클래스)의 *(모든 메서드)가 만족 될 경우 logger.info 내용을 호출
	@Before("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onBeforeHandler(JoinPoint joinPoint) {
		System.out.println("testNoAop() 시작되었다.");
		logger.info("========== onBeforeThing");
	}

	@After("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onAfterHandler(JoinPoint joinPoint) {
		logger.info("========== onAfterHandler");
	}
	
	@Around("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public Object onAround(ProceedingJoinPoint proceedingJoinPoint) {
		logger.info("@Around Start");
		Object result = null;
		try {
			result = proceedingJoinPoint.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		logger.info("@Around End");
		return result;
	}
	
	
	@AfterReturning(pointcut = "onPointcut()")
	public void onAfterReturningByPointcut() {
		logger.info("========== onAfterReturningByPointcut");
	}
	
	@Pointcut("execution(* kr.ac.kopo.ctc.spring.board.service.*.*Aop(..))")
	public void onPointcut() {	}
}
