package kr.ac.kopo.ctc.spring.board.domain;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.service.GongjiService;
import kr.ac.kopo.ctc.spring.board.service.GongjiServiceImpl;

@Entity // DB 테이블 생성
public class Gongji {

	@Id
	@GeneratedValue
	@Column
	private Integer id;

	@Column
	private String author;

	@Column
	private String title;

	@Column
	private String content;

	@Column
	private Date date;

	//User 테이블과 조인
	@ManyToOne(optional = false) // null 허용여부
	@JoinColumn(name = "user_id") // FK
	private User user;

	public Gongji() {
	}

	//getter/setter 설정
	public Gongji(String title, String content) {
		this.title = title;
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		String result = "[gongji_" + id + "]" + title;
		return result;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;

	}

}