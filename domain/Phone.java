package kr.ac.kopo.ctc.spring.board.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Phone {
	
	@Id
	@GeneratedValue
	@Column
	private Integer id;
	
	@Column
	private String no;
	
	//optional=false : null값 허용
	@ManyToOne(optional=false)
	@JoinColumn(name="member_id")
	private Member member;
	
	public Phone(String no) {
		this.no = no;
	}

	@Override 
	public String toString() {
		String result = "[phone_"+id+"]" + no;
		return result;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Integer getId() {
		return id;
	}

	public String getNo() {
		return no;
	}

	public Member getMember() {
		return member;
	}

}
