package kr.ac.kopo.ctc.spring.board.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity // DB 테이블 생성
public class User {
   
   @Id
   @GeneratedValue
   @Column
   private Integer id;
   
   @Column
   private String name;
   
   //JPA의 cascade 옵션: 영속성 전이(부모 엔티티 저장 시 자식 엔티티 저장 옵션)
   @OneToMany(cascade=CascadeType.ALL, mappedBy="user")
   private Collection<Gongji> gongji_list;
   
   public User() {}
   public User(int id) {
      this.id=id;
   }

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   //Array List에 테이블 내용 넣기
   public Collection<Gongji> getGongji_list(){
      if(gongji_list == null) {
    	  gongji_list = new ArrayList<Gongji>();
      }
      return gongji_list;
   }
   
   public void setGongjis(Collection<Gongji> gongji_list) {
      this.gongji_list = gongji_list;
      
   }
   
   public void addGongji(Gongji g) {
      Collection<Gongji>gongji_list = getGongji_list();
      g.setUser(this);
      gongji_list.add(g);
      
   }
   
   @Override
   public String toString() {
      String result = "["+id+"]"+name;
      for(Gongji g : getGongji_list()) {
         result += "\n" + g.toString();
      }
      return result;
   }
}