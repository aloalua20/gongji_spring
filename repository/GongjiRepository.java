package kr.ac.kopo.ctc.spring.board.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;

public interface GongjiRepository extends JpaRepository<Gongji, Integer>{
	Page<Gongji> findAll(Pageable pageable);
	//리스트 분할
	Page<Gongji> findAllByOrderByIdDesc(Pageable pageable);
	Page<Gongji> findAllByAuthor(String author, Pageable pageable);
	Page<Gongji> findAllByUser(User user, Pageable pageable);
	
	//검색 조건에 따른 쿼리 설정
	//제목으로 검색
	@Query("select g from Gongji g where title like concat('%', :keyword, '%')")
	Page<Gongji> findAllSearchTitle(@Param("keyword") String keyword, Pageable pageable);
	//작성자로 검색
	@Query("select t from Gongji t where author like concat('%', :keyword, '%')")
	Page<Gongji> findAllSearchAuthor(@Param("keyword") String keyword, Pageable pageable);
	//내용으로 검색
	@Query("select t from Gongji t where content like concat('%', :keyword, '%')")
	Page<Gongji> findAllSearchContent(@Param("keyword") String keyword, Pageable pageable);
	
	

}
