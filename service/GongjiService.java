package kr.ac.kopo.ctc.spring.board.service;

import java.util.Date;
import java.util.List;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;

//인터페이스인 공지 서비스 
public interface GongjiService {
	//페이지 찾기
	List<Gongji> findAllByPage(int page);
	//전체 보기
	List<Gongji> findAll(int page, int cnt);
	//하나 검색
	Gongji findById(int id);
	//하나 삭제
	void deleteById(int id);
	//페이지 찾기
	GongjiDto findPage(int page, int cnt);
	//내림차순 페이지 찾기
	List<Gongji> findAllByOrderByIdDesc(int page, int cnt);
	//수정
	Gongji update(int id, String title, String author, String content);
	//생성
	Gongji create(String title, int user_id, String content);
	//검색 결과 출력
	GongjiDto listAll(String searchOption, String keyword, int page, int cnt);
	
}
