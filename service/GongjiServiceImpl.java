package kr.ac.kopo.ctc.spring.board.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.domain.User;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;
import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.repository.UserRepository;

//인터페이스인 서비스를 구체화하는 서비스 임플
@Service
public class GongjiServiceImpl implements GongjiService {

	@Autowired
	private GongjiRepository gongjiRepository;

	@Autowired
	private UserRepository userRepository;

	public List<Gongji> findAllByPage(int page) {
		Page<Gongji> pages = gongjiRepository.findAll(PageRequest.of(page, 10));
		return pages.getContent();
	}

	@Override
	public List<Gongji> findAllByOrderByIdDesc(int page, int cnt) {
		return gongjiRepository.findAllByOrderByIdDesc(PageRequest.of(page, cnt)).getContent();
	}

	// 전체 보기
	@Override
	public List<Gongji> findAll(int page, int cnt) {
		return gongjiRepository.findAll(PageRequest.of(page, cnt)).getContent();
	}

	// 게시물 하나 보기 (id로 검색)
	@Override
	public Gongji findById(int id) {
		return gongjiRepository.findById(id).get();
	}

	// 게시물 삭제 (id로 검색)
	@Override
	public void deleteById(int id) {
		gongjiRepository.deleteById(id);
	}

	// 페이지네이션에서 사용 될 변수

	int firstPage = 0; // 표시 될 첫 번째 페이지 수 (e.g. 1, 11, 21, ...)
	int next = 0; // 다음
	int prev = 0; // 이전
	int block = 10; // 한 번에 표시 될 페이지 수 (e.g. 1~10, 11~20, ...)
	int lastPage = 0; // 표시 될 마지막 페이지 수 (e.g. 10, 20, 30, ...)

	// 페이지네이션 메서드
	private GongjiDto pagination(int page, int cnt, Page<Gongji> gongji_list) {
		// 첫 번째 페이지 수
		if (page > 0) {
			page = page - 1;
			firstPage = ((page + 1) / block) * block + 1;
		} else {
			firstPage = 1;
		}
		// getTotalPages()로 총 페이지 수 얻기
		int totalPage = gongji_list.getTotalPages();

		// Dto 객체 생성
		GongjiDto gongjiDto = new GongjiDto();

		// 마지막 페이지 수
		lastPage = firstPage + 9;
		if (lastPage > totalPage) {
			lastPage = totalPage;
		}

		// 이전 버튼
		if (page < firstPage + 1) {
			prev = 1;
		} else {
			prev = firstPage - 10;
		}

		// 다음 버튼
		next = firstPage + 10;
		if (next > totalPage) { // next
			next = totalPage;
		}

		// 각 페이지 버튼에 대한 값을 gongjiDto에 저장
		gongjiDto.setGongjies(gongji_list.getContent());
		gongjiDto.setStart(1);
		gongjiDto.setFirstPage(firstPage);
		gongjiDto.setLastPage(lastPage);
		gongjiDto.setEnd(totalPage);
		gongjiDto.setTotalpage(totalPage);

		gongjiDto.setPrev(prev);
		gongjiDto.setNext(next);

		return gongjiDto;
	}

	// 게시물 전체 리스트 조회 시 페이지네이션
	@Override
	public GongjiDto findPage(int page, int cnt) {
		Page<Gongji> gongji_list = gongjiRepository.findAll(PageRequest.of(page, cnt));
		return pagination(page, cnt, gongji_list);
	}

	// 검색 결과 조회 시 페이지네이션
	@Override
	public GongjiDto listAll(String searchOption, String keyword, int page, int cnt) {
		Page<Gongji> gongji_list = null;
		if (searchOption.equals("title")) {
			gongji_list = gongjiRepository.findAllSearchTitle(keyword, PageRequest.of(page, cnt));
		} else if (searchOption.equals("author")) {
			User u = userRepository.findByName(keyword).get(0);
			gongji_list = gongjiRepository.findAllByUser(u, PageRequest.of(page, cnt));
		} else if (searchOption.equals("content")) {
			gongji_list = gongjiRepository.findAllSearchContent(keyword, PageRequest.of(page, cnt));

		} else {
			gongji_list = Page.empty();
		}
		return pagination(page, cnt, gongji_list);
	}

	// 게시물 수정
	@Override
	public Gongji update(int id, String title, String author, String content) {
		
		//공지게시판 Repository에서 아이디로 검색하여 객체 생성
		Gongji g = gongjiRepository.findById(id).get();
		
		//입력받은 값으로 세팅
		g.setTitle(title);
		g.setDate(new Date());
		g.setAuthor(author);
		g.setContent(content);
		//저장
		gongjiRepository.save(g);

		return g;
	}

	// 게시물 생성
	@Override
	public Gongji create(String title, int user_id, String content) {

		//게시판 도메인 객체 생성
		Gongji g = new Gongji();
		//사용자 Repository에서 사용자 id 받아오기
		User u = userRepository.getOne(user_id);
		System.out.println("u: " + u);
		if (u == null) {
			u = new User(user_id);
			userRepository.save(u);
		}
		//입력 받은 값으로 세팅
		g.setTitle(title);
		g.setDate(new Date());
		g.setContent(content);
		g.setUser(u);
		//저장
		gongjiRepository.save(g);

		return g;
	}

}
