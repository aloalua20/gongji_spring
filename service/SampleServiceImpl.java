package kr.ac.kopo.ctc.spring.board.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.ac.kopo.ctc.spring.board.domain.Sample;
import kr.ac.kopo.ctc.spring.board.repository.SampleRepository;

//비지니스 로직, interface 기반
@Service
public class SampleServiceImpl implements SampleService{
	
	@Autowired
	SampleRepository sampleRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);
	
	//transactional annotation 없으므로 오류 나도 저장 됨
	@Override
	public String testNoTransactional() {
		Sample sample = sampleRepository.findById(1L).get();
		sample.setTitle("update1");
		sampleRepository.save(sample);
		
		throw new RuntimeException("Spring Boot No Transactional Test");
	}
	
	//transactional annotation 있으므로 오류 발생시 저장되지 않음
	@Override
	@Transactional
	public String testTransactional() {
		Sample sample = sampleRepository.findById(1L).get();
		sample.setTitle("update1");
		sampleRepository.save(sample);
		
		throw new RuntimeException("Spring Boot Transactional Test");
	}
	

	//한 메소드에 두 개의 관심사 포함 
	@Override
	public String testNoAop() {
		//관심사 1: 개발시 호출여부 확인 > 관심사 분리를 위해 LogAspect.java 파일로 이동
		//System.out.println("testNoAop() 시작되었다."); 
		
		//관심사 2: 비지니스 로직
		String msg = "Hello, Spring Boot No AOP";
		logger.info(msg);
		return msg;
	}
	
	@Override
	public String testAop() {
		String msg = "Hello, Spring Boot AOP";
		logger.info(msg);
		return msg;
	}
}
