package kr.ac.kopo.ctc.spring.board.web;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import kr.ac.kopo.ctc.spring.board.domain.Gongji;
import kr.ac.kopo.ctc.spring.board.dto.GongjiDto;
import kr.ac.kopo.ctc.spring.board.repository.GongjiRepository;
import kr.ac.kopo.ctc.spring.board.service.GongjiService;

//컨트롤러 
@Controller
public class GongjiController {

	@Autowired
	private GongjiService gongjiService;

	@Autowired
	private GongjiRepository gongjiRepository;

	// CRUD
	// findAll : 전체 리스트 보기
	@RequestMapping(value = "/gongji/findAll")
	public String findAll(Model model, @RequestParam HashMap<String, String> map) {

		//페이지 가져오기
		String page = map.get("page");
		//페이지값이 없으면
		if (page == null) {
			//페이지는 1
			page = "1";
		}
		//검색에 관련 된 searchOption(검색조건)과 keyword(검색 키워드)가져오기
		String keyword = map.get("keyword");
		String searchOption = map.get("searchOption");
		int pagei = Integer.parseInt(page);
		//키워드 값이 있을 경우 (=검색 결과를 조회하는 경우)
		if (keyword != null) {
			//검색 결과 조회 시 페이지네이션
			GongjiDto dto = gongjiService.listAll(searchOption, keyword, pagei - 1, 10);
			//모델에 값 저장
			model.addAttribute("keyword", keyword);
			model.addAttribute("searchOption", searchOption);
			model.addAttribute("dto", dto);
		} else {
			//키워드 값이 없을 경우 (=게시물 전체 조회 시 페이지네이션)
			GongjiDto dto = gongjiService.findPage(Integer.parseInt(page) - 1, 10);

			model.addAttribute("dto", dto);
		}

		return "gongji/findAll";
	}

	//findById : 게시물 하나 보기
	@RequestMapping(value = "/gongji/findById")
	public String findById(Model model, @RequestParam HashMap<String, String> map) {
		
		//id 가져와서 모델에 값 저장
		String ids = map.get("id");
		int id = Integer.parseInt(ids);
		Gongji dto = gongjiService.findById(id);
		model.addAttribute("dto", dto);

		return "gongji/findById";
	}

	// update: 게시글 수정
	@RequestMapping(value = "/gongji/update")
	public String update(Model model, @RequestParam HashMap<String, String> map) {

		String ids = map.get("id");
		int id = Integer.parseInt(ids);
		Gongji dto = gongjiService.findById(id);
		Date date = new Date();
		model.addAttribute("date", date);
		model.addAttribute("dto", dto);

		return "gongji/update";
	}

	// delete : 게시물 삭제
	@RequestMapping(value = "/gongji/delete")
	public void deleteById(Model model, @RequestParam HashMap<String, String> map) {

		String ids = map.get("id");
		int id = Integer.parseInt(ids);
		gongjiService.deleteById(id);

	}

	// create : 게시물 작성
	@RequestMapping(value = "/gongji/create")
	public String create(Model model, @RequestParam HashMap<String, String> map) {

		Date date = new Date();
		model.addAttribute("date", date);

		return "gongji/create";
	}

	// write : create와 update에서 폼으로 받은 값을 처리
	@RequestMapping(value = "/gongji/write")
	public String write(Model model, @RequestParam HashMap<String, String> map) {

		//폼에서 받은 값 가져오기
		String ids = map.get("id");
		System.out.println(ids);

		String title = map.get("title");
		String author = map.get("author");
		String content = map.get("content");
		String date = map.get("date");
		String user_id = map.get("user_id");

		//게시글 id가 있는 경우 (= 게시물 수정하는 경우) - update
		if (ids != null) {
			int id = Integer.parseInt(ids);
			gongjiService.update(id, title, author, content);
		} else {
			//게시글 id가 없을 경우 (=신규 글 작성일 경우) - create
			//title, user_id, content를 저장 
			gongjiService.create(title, Integer.parseInt(user_id), content);
		}
		// 전체보기 페이지로 리다이렉트
		return "redirect:/gongji/findAll";
	}

	// 게시물 검색
	@RequestMapping("value=gongji/findAll")
	public ModelAndView list(@RequestParam(defaultValue = "title") String searchOption,
			@RequestParam(defaultValue = "") String keyword, @RequestParam(defaultValue = "0") int page) {

		//서비스에서 지정 한 검색 결과 보기(listAll)의 객체 생성
		GongjiDto dto = gongjiService.listAll(searchOption, keyword, 10, 10);
		ModelAndView mav = new ModelAndView();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("searchOption", searchOption);
		map.put("keyword", keyword);
		map.put("searchOption", searchOption);
		mav.addObject("map", map);

		return mav;
	}

}
