package kr.ac.kopo.ctc.spring.board.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//controller 생성; controller는 model(data)과 view(.jsp)를 연결
@Controller
public class HelloController {
	//url input(입력 주소) 생성 
	@RequestMapping(value="/hello")
	public String hellSpringBoot(Model model) {
		//key와 value
		model.addAttribute("name", "홍길동");
		/*출력 경로: 
		application.properties 설정대로 
		value를 중심으로 prefix,suffix 적용하여 출력경로 지정
		(/WEB-INF/views/hello.jsp)
		*/
		return "hello";
	}

}

