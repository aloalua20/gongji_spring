package kr.ac.kopo.ctc.spring.board.web;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import kr.ac.kopo.ctc.spring.board.service.SampleCacheService;

@Controller
public class SampleCacheController {
	
	//private static final Logger=LoggerFactory.getLogger(SampleCacheController.class);
	
	@Autowired
	private SampleCacheService sampleCashService;
	
	@RequestMapping(value = "/sample/noCache")
	@ResponseBody
	public String noCache(@RequestParam HashMap<String, String> map) {
		Long id = Long.parseLong(map.get("id"));
		return sampleCashService.testNoCache(id);
	}
	
	@RequestMapping(value="/sample/cache")
	@ResponseBody
	public String cache(@RequestParam HashMap<String, String> map) {
		Long id = Long.parseLong(map.get("id"));
		return sampleCashService.testCache(id);
	}

}
